package model;

public class Generator {
    public static Book[] generate() {
        Book[] books = new Book[6];
        // generation
        books[0] = new Book("Effective Java: Programming Language Guide", "Joshua Bloch", 2001, "Addison Wesley");
        books[1] = new Book("Thinking in Java", "Bruce Eckel", 2009, "New Jersy");
        books[2] = new Book("Тонкости программирования", "1: Кей С. Хортсманн; 2: Гари Корнелл", 2007, "Издательский дом 'Вильямс'");
        books[3] = new Book("Основы программирования", "1: Кей С. Хортсманн; 2: Гари Корнелл", 2007, "Киев");
        books[4] = new Book("Полное руководство Java", "Геберт Шилд", 2012, "Киев");
        books[5] = new Book("Язык программирования Java", "1: Кен Арнольд; 2: Джеймс Гослинг; 3: Гари Корнелл ", 2006, "Москва");
        return books;
    }
}
